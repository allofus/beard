# Christmas Beard
Beard!

## Summary
Beard?

## Local Development
1.  Install Raspbian OS
2.  Update firmware sudo `rpi-update`
3.  Follow this [tuorial](http://natemcbean.com/2012/11/rpi-thermal-printer/) to setup serial port but don't download and install the printer library from git yet

WARNING! In the above tutorial, root=/dev/mmcblk0p2 destroys your boot sequence, change the p2 to p6 like so:
`dwc_otg.lpm_enable=0 console=tty1 root=/dev/mmcblk0p6 rootfstype=ext4 rootwait`

### Setup
1. mkdir projects in ~/ and cd into it
1. `git clone git@bitbucket.org/allofus/beard`
1. cd into beard and clone thernmal printer lib `git clone git://github.com/luopio/py-thermal-printer.git pythermal`
1. run it `python beard.py`

## Authors
- [Chris](mailto:chris@allofus.com)